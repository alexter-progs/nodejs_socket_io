const expect = require('expect');

const { isRealString } = require('./validations');

describe('isRealString', () => {
    it('should reject non-string values', () => {
        expect(isRealString(4)).toBe(false);
    });

    it('should reject string with length equal to 0', () => {
        expect(isRealString('')).toBe(false);
    });

    it('should reject string with only spaces', () => {
        expect(isRealString('         ')).toBe(false);
    });

    it('should pass string with length > 0', () => {
        expect(isRealString('   Normal string  ')).toBe(true);
    });
});