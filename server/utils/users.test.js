const expect = require('expect');

const { Users } = require('./users');

describe('Users', () => {
    let users;

    beforeEach(() => {
        users = new Users();
        users.users = [{
            id: '1',
            name: 'Mike',
            room: 'Node course'
        }, {
            id: '2',
            name: 'Jen',
            room: 'React course'
        }, {
            id: '3',
            name: 'Julie',
            room: 'Node course'
        }]
    })

    it('should add new user', () => {
        let users = new Users();

        let user = {
            id: '1',
            name: 'Alexander',
            room: 'Devs'
        }

        let resUser = users.addUser(user.id, user.name, user.room);

        expect(users.users).toEqual([user]);
    });

    it('should return names for node course', () => {
        let userList = users.getUserList('Node course');

        expect(userList).toEqual(['Mike', 'Julie'])
    });

    it('should return names for react course', () => {
        let userList = users.getUserList('React course');

        expect(userList).toEqual(['Jen'])
    });

    it('should remove user', () => {
        let userId = '1';
        let user = users.removeUser(userId);

        expect(user.id).toBe(userId);
        expect(users.users.length).toBe(2);
    });

    it('should not remove user', () => {
        let userId = 'Not an ID';
        let user = users.removeUser(userId);

        expect(user).toNotExist();
        expect(users.users.length).toBe(3);
    })

    it('should find a user', () => {
        let userId = '2';
        let user = users.getUser(userId);

        expect(user.id).toBe('2');
    })

    it('should not find a user', () => {
        let userId = 'Not an ID';
        let user = users.getUser(userId);

        expect(user).toBe(undefined);
    })
});